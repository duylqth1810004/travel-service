﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TravelServiceAssignment_Consumer.TravelService;

namespace TravelServiceAssignment_Consumer.Controllers
{
    public class OrderTourController : Controller
    {
        TravelServiceClient tsc = new TravelServiceClient();
        // GET: OrderTour
        public ActionResult Index()
        {
            return View();
        }

        // GET: OrderTour/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        public ActionResult Success()
        {
            return View();
        }

        public ActionResult Failed()
        {
            return View();
        }

        // POST: OrderTour/Create
        [HttpPost]
        public ActionResult Create()
        {
            OrderTour ot = new OrderTour();

            ot.CustomerEmail = HttpContext.Request.Form["CustomerEmail"];
            ot.CustomerName = HttpContext.Request.Form["CustomerName"];
            ot.CustomerPhoneNumber = HttpContext.Request.Form["CustomerPhoneNumber"];
            ot.DayDuration = int.Parse(HttpContext.Request.Form["DayDuration"]);
            ot.PeopleAmount = int.Parse(HttpContext.Request.Form["PeopleAmount"]);
            ot.TourId = int.Parse(HttpContext.Request.Form["TourId"]);

            bool status = tsc.AddOrderTour(ot);

            if (status)
            {
                return RedirectToAction("Success", "OrderTour");
            }
            else
            {
                return RedirectToAction("Failed", "OrderTour");
            }
        }

        // GET: OrderTour/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: OrderTour/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: OrderTour/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: OrderTour/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
