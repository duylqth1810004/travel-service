﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TravelServiceAssignment_Consumer.TravelService;

namespace TravelServiceAssignment_Consumer.Controllers
{
    public class TourController : Controller
    {
        TravelServiceClient tsc = new TravelServiceClient();
        // GET: Tour
        public ActionResult Index()
        {
            ViewBag.listTours = tsc.GetTourList();
            return View();
        }

        // GET: Tour/Details/5
        public ActionResult Details(int id)
        {
            var tour = tsc.GetTourById(id);
            return View(tour);
        }

        // POST: Tour/Create
        [HttpPost]
        public ActionResult Create(OrderTour ot)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Tour/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Tour/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Tour/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Tour/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
