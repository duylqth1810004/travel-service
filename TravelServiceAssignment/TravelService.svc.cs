﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace TravelServiceAssignment
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "TravelService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select TravelService.svc or TravelService.svc.cs at the Solution Explorer and start debugging.
    public class TravelService : ITravelService
    {
        TravelServiceDBDataContext data = new TravelServiceDBDataContext();
        public Tour GetTourById(int id)
        {
            try
            {
                return (from tour in data.Tours
                        where tour.TourId == id
                        select tour).SingleOrDefault();
            }
            catch
            {
                return null;
            }
        }

        public Tour GetTourByName(string tourName)
        {
            try
            {
                return (from tour in data.Tours
                        where tour.Name.Contains(tourName)
                        select tour).SingleOrDefault();
            }
            catch
            {
                return null;
            }
        }

        public Tour GetTourByPlace(string placeName)
        {
            try
            {
                return (from tour in data.Tours
                        where tour.Place.Contains(placeName)
                        select tour).SingleOrDefault();
            }
            catch
            {
                return null;
            }
        }

        public List<Tour> GetTourList()
        {
            try
            {
                return (from tour in data.Tours select tour).ToList();
            }
            catch
            {
                return null;
            }
        }

        public bool AddOrderTour(OrderTour ot)
        {
            try
            {
                data.OrderTours.InsertOnSubmit(ot);
                data.SubmitChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
