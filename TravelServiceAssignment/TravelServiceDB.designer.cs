﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TravelServiceAssignment
{
	using System.Data.Linq;
	using System.Data.Linq.Mapping;
	using System.Data;
	using System.Collections.Generic;
	using System.Reflection;
	using System.Linq;
	using System.Linq.Expressions;
	using System.Runtime.Serialization;
	using System.ComponentModel;
	using System;
	
	
	[global::System.Data.Linq.Mapping.DatabaseAttribute(Name="TravelServiceDB")]
	public partial class TravelServiceDBDataContext : System.Data.Linq.DataContext
	{
		
		private static System.Data.Linq.Mapping.MappingSource mappingSource = new AttributeMappingSource();
		
    #region Extensibility Method Definitions
    partial void OnCreated();
    partial void InsertOrderTour(OrderTour instance);
    partial void UpdateOrderTour(OrderTour instance);
    partial void DeleteOrderTour(OrderTour instance);
    partial void InsertTour(Tour instance);
    partial void UpdateTour(Tour instance);
    partial void DeleteTour(Tour instance);
    #endregion
		
		public TravelServiceDBDataContext() : 
				base(global::System.Configuration.ConfigurationManager.ConnectionStrings["TravelServiceDBConnectionString"].ConnectionString, mappingSource)
		{
			OnCreated();
		}
		
		public TravelServiceDBDataContext(string connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public TravelServiceDBDataContext(System.Data.IDbConnection connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public TravelServiceDBDataContext(string connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public TravelServiceDBDataContext(System.Data.IDbConnection connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public System.Data.Linq.Table<OrderTour> OrderTours
		{
			get
			{
				return this.GetTable<OrderTour>();
			}
		}
		
		public System.Data.Linq.Table<Tour> Tours
		{
			get
			{
				return this.GetTable<Tour>();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.OrderTour")]
	[global::System.Runtime.Serialization.DataContractAttribute()]
	public partial class OrderTour : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private int _OrderId;
		
		private int _PeopleAmount;
		
		private int _DayDuration;
		
		private string _CustomerName;
		
		private string _CustomerPhoneNumber;
		
		private string _CustomerEmail;
		
		private int _TourId;
		
		private EntityRef<Tour> _Tour;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnOrderIdChanging(int value);
    partial void OnOrderIdChanged();
    partial void OnPeopleAmountChanging(int value);
    partial void OnPeopleAmountChanged();
    partial void OnDayDurationChanging(int value);
    partial void OnDayDurationChanged();
    partial void OnCustomerNameChanging(string value);
    partial void OnCustomerNameChanged();
    partial void OnCustomerPhoneNumberChanging(string value);
    partial void OnCustomerPhoneNumberChanged();
    partial void OnCustomerEmailChanging(string value);
    partial void OnCustomerEmailChanged();
    partial void OnTourIdChanging(int value);
    partial void OnTourIdChanged();
    #endregion
		
		public OrderTour()
		{
			this.Initialize();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_OrderId", AutoSync=AutoSync.OnInsert, DbType="Int NOT NULL IDENTITY", IsPrimaryKey=true, IsDbGenerated=true)]
		[global::System.Runtime.Serialization.DataMemberAttribute(Order=1)]
		public int OrderId
		{
			get
			{
				return this._OrderId;
			}
			set
			{
				if ((this._OrderId != value))
				{
					this.OnOrderIdChanging(value);
					this.SendPropertyChanging();
					this._OrderId = value;
					this.SendPropertyChanged("OrderId");
					this.OnOrderIdChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_PeopleAmount", DbType="Int NOT NULL")]
		[global::System.Runtime.Serialization.DataMemberAttribute(Order=2)]
		public int PeopleAmount
		{
			get
			{
				return this._PeopleAmount;
			}
			set
			{
				if ((this._PeopleAmount != value))
				{
					this.OnPeopleAmountChanging(value);
					this.SendPropertyChanging();
					this._PeopleAmount = value;
					this.SendPropertyChanged("PeopleAmount");
					this.OnPeopleAmountChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_DayDuration", DbType="Int NOT NULL")]
		[global::System.Runtime.Serialization.DataMemberAttribute(Order=3)]
		public int DayDuration
		{
			get
			{
				return this._DayDuration;
			}
			set
			{
				if ((this._DayDuration != value))
				{
					this.OnDayDurationChanging(value);
					this.SendPropertyChanging();
					this._DayDuration = value;
					this.SendPropertyChanged("DayDuration");
					this.OnDayDurationChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_CustomerName", DbType="NVarChar(50) NOT NULL", CanBeNull=false)]
		[global::System.Runtime.Serialization.DataMemberAttribute(Order=4)]
		public string CustomerName
		{
			get
			{
				return this._CustomerName;
			}
			set
			{
				if ((this._CustomerName != value))
				{
					this.OnCustomerNameChanging(value);
					this.SendPropertyChanging();
					this._CustomerName = value;
					this.SendPropertyChanged("CustomerName");
					this.OnCustomerNameChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_CustomerPhoneNumber", DbType="NVarChar(15) NOT NULL", CanBeNull=false)]
		[global::System.Runtime.Serialization.DataMemberAttribute(Order=5)]
		public string CustomerPhoneNumber
		{
			get
			{
				return this._CustomerPhoneNumber;
			}
			set
			{
				if ((this._CustomerPhoneNumber != value))
				{
					this.OnCustomerPhoneNumberChanging(value);
					this.SendPropertyChanging();
					this._CustomerPhoneNumber = value;
					this.SendPropertyChanged("CustomerPhoneNumber");
					this.OnCustomerPhoneNumberChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_CustomerEmail", DbType="NVarChar(30) NOT NULL", CanBeNull=false)]
		[global::System.Runtime.Serialization.DataMemberAttribute(Order=6)]
		public string CustomerEmail
		{
			get
			{
				return this._CustomerEmail;
			}
			set
			{
				if ((this._CustomerEmail != value))
				{
					this.OnCustomerEmailChanging(value);
					this.SendPropertyChanging();
					this._CustomerEmail = value;
					this.SendPropertyChanged("CustomerEmail");
					this.OnCustomerEmailChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_TourId", DbType="Int NOT NULL")]
		[global::System.Runtime.Serialization.DataMemberAttribute(Order=7)]
		public int TourId
		{
			get
			{
				return this._TourId;
			}
			set
			{
				if ((this._TourId != value))
				{
					if (this._Tour.HasLoadedOrAssignedValue)
					{
						throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
					}
					this.OnTourIdChanging(value);
					this.SendPropertyChanging();
					this._TourId = value;
					this.SendPropertyChanged("TourId");
					this.OnTourIdChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.AssociationAttribute(Name="Tour_OrderTour", Storage="_Tour", ThisKey="TourId", OtherKey="TourId", IsForeignKey=true)]
		public Tour Tour
		{
			get
			{
				return this._Tour.Entity;
			}
			set
			{
				Tour previousValue = this._Tour.Entity;
				if (((previousValue != value) 
							|| (this._Tour.HasLoadedOrAssignedValue == false)))
				{
					this.SendPropertyChanging();
					if ((previousValue != null))
					{
						this._Tour.Entity = null;
						previousValue.OrderTours.Remove(this);
					}
					this._Tour.Entity = value;
					if ((value != null))
					{
						value.OrderTours.Add(this);
						this._TourId = value.TourId;
					}
					else
					{
						this._TourId = default(int);
					}
					this.SendPropertyChanged("Tour");
				}
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
		
		private void Initialize()
		{
			this._Tour = default(EntityRef<Tour>);
			OnCreated();
		}
		
		[global::System.Runtime.Serialization.OnDeserializingAttribute()]
		[global::System.ComponentModel.EditorBrowsableAttribute(EditorBrowsableState.Never)]
		public void OnDeserializing(StreamingContext context)
		{
			this.Initialize();
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.Tour")]
	[global::System.Runtime.Serialization.DataContractAttribute()]
	public partial class Tour : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private int _TourId;
		
		private string _Name;
		
		private string _Place;
		
		private double _BasePrice;
		
		private string _Description;
		
		private EntitySet<OrderTour> _OrderTours;
		
		private bool serializing;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnTourIdChanging(int value);
    partial void OnTourIdChanged();
    partial void OnNameChanging(string value);
    partial void OnNameChanged();
    partial void OnPlaceChanging(string value);
    partial void OnPlaceChanged();
    partial void OnBasePriceChanging(double value);
    partial void OnBasePriceChanged();
    partial void OnDescriptionChanging(string value);
    partial void OnDescriptionChanged();
    #endregion
		
		public Tour()
		{
			this.Initialize();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_TourId", AutoSync=AutoSync.OnInsert, DbType="Int NOT NULL IDENTITY", IsPrimaryKey=true, IsDbGenerated=true)]
		[global::System.Runtime.Serialization.DataMemberAttribute(Order=1)]
		public int TourId
		{
			get
			{
				return this._TourId;
			}
			set
			{
				if ((this._TourId != value))
				{
					this.OnTourIdChanging(value);
					this.SendPropertyChanging();
					this._TourId = value;
					this.SendPropertyChanged("TourId");
					this.OnTourIdChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Name", DbType="NVarChar(50) NOT NULL", CanBeNull=false)]
		[global::System.Runtime.Serialization.DataMemberAttribute(Order=2)]
		public string Name
		{
			get
			{
				return this._Name;
			}
			set
			{
				if ((this._Name != value))
				{
					this.OnNameChanging(value);
					this.SendPropertyChanging();
					this._Name = value;
					this.SendPropertyChanged("Name");
					this.OnNameChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Place", DbType="NVarChar(100) NOT NULL", CanBeNull=false)]
		[global::System.Runtime.Serialization.DataMemberAttribute(Order=3)]
		public string Place
		{
			get
			{
				return this._Place;
			}
			set
			{
				if ((this._Place != value))
				{
					this.OnPlaceChanging(value);
					this.SendPropertyChanging();
					this._Place = value;
					this.SendPropertyChanged("Place");
					this.OnPlaceChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_BasePrice", DbType="Float NOT NULL")]
		[global::System.Runtime.Serialization.DataMemberAttribute(Order=4)]
		public double BasePrice
		{
			get
			{
				return this._BasePrice;
			}
			set
			{
				if ((this._BasePrice != value))
				{
					this.OnBasePriceChanging(value);
					this.SendPropertyChanging();
					this._BasePrice = value;
					this.SendPropertyChanged("BasePrice");
					this.OnBasePriceChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Description", DbType="NVarChar(100)")]
		[global::System.Runtime.Serialization.DataMemberAttribute(Order=5)]
		public string Description
		{
			get
			{
				return this._Description;
			}
			set
			{
				if ((this._Description != value))
				{
					this.OnDescriptionChanging(value);
					this.SendPropertyChanging();
					this._Description = value;
					this.SendPropertyChanged("Description");
					this.OnDescriptionChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.AssociationAttribute(Name="Tour_OrderTour", Storage="_OrderTours", ThisKey="TourId", OtherKey="TourId")]
		[global::System.Runtime.Serialization.DataMemberAttribute(Order=6, EmitDefaultValue=false)]
		public EntitySet<OrderTour> OrderTours
		{
			get
			{
				if ((this.serializing 
							&& (this._OrderTours.HasLoadedOrAssignedValues == false)))
				{
					return null;
				}
				return this._OrderTours;
			}
			set
			{
				this._OrderTours.Assign(value);
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
		
		private void attach_OrderTours(OrderTour entity)
		{
			this.SendPropertyChanging();
			entity.Tour = this;
		}
		
		private void detach_OrderTours(OrderTour entity)
		{
			this.SendPropertyChanging();
			entity.Tour = null;
		}
		
		private void Initialize()
		{
			this._OrderTours = new EntitySet<OrderTour>(new Action<OrderTour>(this.attach_OrderTours), new Action<OrderTour>(this.detach_OrderTours));
			OnCreated();
		}
		
		[global::System.Runtime.Serialization.OnDeserializingAttribute()]
		[global::System.ComponentModel.EditorBrowsableAttribute(EditorBrowsableState.Never)]
		public void OnDeserializing(StreamingContext context)
		{
			this.Initialize();
		}
		
		[global::System.Runtime.Serialization.OnSerializingAttribute()]
		[global::System.ComponentModel.EditorBrowsableAttribute(EditorBrowsableState.Never)]
		public void OnSerializing(StreamingContext context)
		{
			this.serializing = true;
		}
		
		[global::System.Runtime.Serialization.OnSerializedAttribute()]
		[global::System.ComponentModel.EditorBrowsableAttribute(EditorBrowsableState.Never)]
		public void OnSerialized(StreamingContext context)
		{
			this.serializing = false;
		}
	}
}
#pragma warning restore 1591
